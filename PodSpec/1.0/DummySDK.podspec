Pod::Spec.new do |s|
  s.name         = 'DummySDK'
  s.version      = '1.0'
  s.summary      = 'Lightweight ad mediation for iOS'
  s.author = {
    'Lars Anderson' => 'youremail@here.com'
  }
  s.source = {
    :git => 'https://grantspilsbury@bitbucket.org/grantspilsbury/dummysdk',
    :tag => '1.0'
  }
  s.source_files = 'Source/*.{h,m}'
  s.homepage     = "http://www.test.com"
  s.license      = "MIT"
  s.license      = { :type => "MIT", :file => "license.txt" }
  s.requires_arc = true
end
